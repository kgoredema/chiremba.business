package com.softhub.chiremba.business.supplier;

import com.softhub.chiremba.business.model.AppService;

public interface SupplierService extends AppService<Supplier> {
}
