package com.softhub.chiremba.business.supplier;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by kelvin on 2019/06/02.
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository repository;

    @Override
    public Optional<Supplier> get(Object id) {
        return repository.findById((Long) id);
    }

    @Override
    public List<Supplier> getAll() {
        return repository.findAll();
    }

    @Override
    public Supplier save(Supplier supplier) {
        return repository.save(supplier);
    }

    @Override
    public void delete(Supplier supplier) {
        repository.delete(supplier);
    }
}
