package com.softhub.chiremba.business.supplier;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kelvin on 2019/05/31.
 */
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
