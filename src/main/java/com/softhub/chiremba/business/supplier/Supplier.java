package com.softhub.chiremba.business.supplier;

import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.supplieraddress.SupplierAddress;
import com.softhub.chiremba.business.suppliercontact.SupplierContact;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "supplier")
public class Supplier extends BaseEntity {

    private String firstName;
    private String lastName;
    private String supplierNumber;
    private boolean active;
    @OneToMany(mappedBy = "supplier")
    private Set<SupplierAddress> supplierAddress;
    @OneToMany(mappedBy = "supplier")
    private Set<SupplierContact> supplierContact;
}
