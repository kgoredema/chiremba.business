package com.softhub.chiremba.business.drug;


import com.softhub.chiremba.business.addresstype.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DrugServiceImpl implements DrugService {

    private final DrugRepository drugRepository;


    @Override
    public Optional<Drug> get(Object id) {
        return drugRepository.findById((Long) id);
    }

    @Override
    public List<Drug> getAll() {
        return drugRepository.findAll();
    }

    @Override
    public Drug save(Drug drug) {
        return drugRepository.save(drug);
    }

    @Override
    public void delete(Drug drug) {
        drugRepository.delete(drug);

    }
}
