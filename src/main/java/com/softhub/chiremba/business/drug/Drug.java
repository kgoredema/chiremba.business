package com.softhub.chiremba.business.drug;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "drug")
public class Drug extends BaseEntity {
    private String name;
    private String description;
    
}
