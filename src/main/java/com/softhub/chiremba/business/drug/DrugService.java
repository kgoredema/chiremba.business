package com.softhub.chiremba.business.drug;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DrugService extends AppService<Drug> {

}
