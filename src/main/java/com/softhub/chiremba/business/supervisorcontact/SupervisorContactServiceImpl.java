package com.softhub.chiremba.business.supervisorcontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SupervisorContactServiceImpl implements SupervisorContactService {

    private final SupervisorContactRepository supervisorContactRepository;


    @Override
    public Optional<SupervisorContact> get(Object id) {
        return supervisorContactRepository.findById((Long) id);
    }

    @Override
    public List<SupervisorContact> getAll() {
        return supervisorContactRepository.findAll();
    }

    @Override
    public SupervisorContact save(SupervisorContact supervisorContact) {
        return supervisorContactRepository.save(supervisorContact);
    }

    @Override
    public void delete(SupervisorContact supervisorContact) {
        supervisorContactRepository.delete(supervisorContact);

    }
}
