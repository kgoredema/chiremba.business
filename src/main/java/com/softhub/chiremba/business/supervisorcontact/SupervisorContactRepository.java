package com.softhub.chiremba.business.supervisorcontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SupervisorContactRepository extends JpaRepository<SupervisorContact, Long> {

}
