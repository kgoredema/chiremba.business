package com.softhub.chiremba.business.supervisorcontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SupervisorContactService extends AppService<SupervisorContact> {

}
