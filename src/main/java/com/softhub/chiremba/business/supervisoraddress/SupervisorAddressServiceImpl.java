package com.softhub.chiremba.business.supervisoraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SupervisorAddressServiceImpl implements SupervisorAddressService {

    private final SupervisorAddressRepository supervisorAddressRepository;


    @Override
    public Optional<SupervisorAddress> get(Object id) {
        return supervisorAddressRepository.findById((Long) id);
    }

    @Override
    public List<SupervisorAddress> getAll() {
        return supervisorAddressRepository.findAll();
    }

    @Override
    public SupervisorAddress save(SupervisorAddress supervisorAddress) {
        return supervisorAddressRepository.save(supervisorAddress);
    }

    @Override
    public void delete(SupervisorAddress supervisorAddress) {
        supervisorAddressRepository.delete(supervisorAddress);

    }
}
