package com.softhub.chiremba.business.supervisoraddress;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.supervisor.Supervisor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "supervisor_address")
public class SupervisorAddress extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Supervisor supervisor;

}
