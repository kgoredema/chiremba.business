package com.softhub.chiremba.business.supervisoraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SupervisorAddressService extends AppService<SupervisorAddress> {

}
