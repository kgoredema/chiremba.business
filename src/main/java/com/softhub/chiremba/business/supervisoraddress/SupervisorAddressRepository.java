package com.softhub.chiremba.business.supervisoraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SupervisorAddressRepository extends JpaRepository<SupervisorAddress, Long> {

}
