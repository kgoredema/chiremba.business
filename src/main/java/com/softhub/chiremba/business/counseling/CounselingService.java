package com.softhub.chiremba.business.counseling;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CounselingService extends AppService<Counseling> {

}
