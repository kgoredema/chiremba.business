package com.softhub.chiremba.business.counseling;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CounselingRepository extends JpaRepository<Counseling, Long> {

}
