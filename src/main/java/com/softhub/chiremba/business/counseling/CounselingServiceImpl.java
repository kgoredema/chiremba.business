package com.softhub.chiremba.business.counseling;


import com.softhub.chiremba.business.addresstype.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CounselingServiceImpl implements CounselingService {

    private final CounselingRepository counselingRepository;


    @Override
    public Optional<Counseling> get(Object id) {
        return counselingRepository.findById((Long) id);
    }

    @Override
    public List<Counseling> getAll() {
        return counselingRepository.findAll();
    }

    @Override
    public Counseling save(Counseling counseling) {
        return counselingRepository.save(counseling);
    }

    @Override
    public void delete(Counseling counseling) {
        counselingRepository.delete(counseling);

    }
}
