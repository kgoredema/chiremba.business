package com.softhub.chiremba.business.counseling;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "counseling")
public class Counseling extends BaseEntity {

    private String name;
    private String description;

}
