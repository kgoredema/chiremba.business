package com.softhub.chiremba.business.debtorcontact;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.debtor.Debtor;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "debtor_contact")
public class DebtorContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Debtor debtor;

}
