package com.softhub.chiremba.business.debtorcontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DebtorContactService extends AppService<DebtorContact> {

}
