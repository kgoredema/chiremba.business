package com.softhub.chiremba.business.debtorcontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DebtorContactServiceImpl implements DebtorContactService {

    private final DebtorContactRepository debtorContactRepository;


    @Override
    public Optional<DebtorContact> get(Object id) {
        return debtorContactRepository.findById((Long) id);
    }

    @Override
    public List<DebtorContact> getAll() {
        return debtorContactRepository.findAll();
    }

    @Override
    public DebtorContact save(DebtorContact debtorContact) {
        return debtorContactRepository.save(debtorContact);
    }

    @Override
    public void delete(DebtorContact debtorContact) {
        debtorContactRepository.delete(debtorContact);

    }
}
