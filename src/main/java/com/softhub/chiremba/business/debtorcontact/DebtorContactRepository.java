package com.softhub.chiremba.business.debtorcontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DebtorContactRepository extends JpaRepository<DebtorContact, Long> {

}
