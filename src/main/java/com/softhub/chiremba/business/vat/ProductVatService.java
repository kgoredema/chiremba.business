package com.softhub.chiremba.business.vat;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by kelvin on 2019/06/13.
 */
public interface ProductVatService  extends AppService<ProductVat>{
}
