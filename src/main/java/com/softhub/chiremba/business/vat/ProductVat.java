package com.softhub.chiremba.business.vat;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * Created by kelvin on 2019/06/13.
 */

@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "product_vat")
@Data
public class ProductVat extends BaseEntity{

    private BigDecimal vatRate;
    private BigDecimal vatLevel;
    private String  description;
}
