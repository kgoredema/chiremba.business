package com.softhub.chiremba.business.vat;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by kelvin on 2019/06/13.
 */

@RequiredArgsConstructor
@Slf4j
@Service
public class ProductVatServiceImpl  implements  ProductVatService{
    private final ProductVatRepository productVatRepository;

    @Override
    public Optional<ProductVat> get(Object id) {
        return  productVatRepository.findById((Long)id);
    }

    @Override
    public List<ProductVat> getAll() {
        return productVatRepository.findAll();
    }

    @Override
    public ProductVat save(ProductVat productVat) {
        return productVatRepository.save(productVat);
    }

    @Override
    public void delete(ProductVat productVat) {
        productVatRepository.delete(productVat);
    }
}
