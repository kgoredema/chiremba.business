package com.softhub.chiremba.business.vat;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kelvin on 2019/06/13.
 */
public interface ProductVatRepository extends JpaRepository<ProductVat,Long>{
}
