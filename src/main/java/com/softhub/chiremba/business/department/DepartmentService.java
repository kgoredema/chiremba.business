package com.softhub.chiremba.business.department;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DepartmentService extends AppService<Department> {

}
