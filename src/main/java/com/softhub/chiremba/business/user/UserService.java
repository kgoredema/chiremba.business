package com.softhub.chiremba.business.user;

import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService extends UserDetailsService {

    User save(User user, Set<Role> newRoles);

    List<User> findAll();

    Optional<User> findById(Long id);

    Optional<User> findByUserName(String username);

    Optional<User> findCurrentUser();

    void updatePassword(String password, Long userId);

    Optional<User> findByEmail(String emailAddress);

    Optional<User> saveUserOnRegistration(User  user);
}
