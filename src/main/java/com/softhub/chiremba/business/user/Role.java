package com.softhub.chiremba.business.user;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Role {

    CHANGE_OTHER_USERS_DETAILS("CHANGE_OTHER_USERS_DETAILS"),
    APPROVAL_ACCESS("APPROVAL_ACCESS"),
    SIS_REPORTS_ACCESS("SIS_REPORTS_ACCESS"),
    SIS_ADMIN_ACCESS("SIS_ADMIN_ACCESS"),
    TECH_OPS("TECH_OPS"),
    INITIATE_BATCH("INITIATE_BATCH");

    private final String roleName;
    public static final String PREFIX = "ROLE_";

    Role(String roleName) {
        this.roleName = roleName;
    }

    public static List<Role> asList() {
        return Arrays.asList(Role.values());
    }

    public String getRoleName() {
        return roleName;
    }

    public String getRoleNameWithPrefix() {
        return PREFIX + roleName;
    }

    @Override
    public String toString() {
        return roleName;
    }

    public static Optional<Role> getRoleForString(String roleName) {

        if (StringUtils.isEmpty(roleName)) {
            return Optional.empty();
        }

        return Arrays.stream(values())
                .filter(r -> r.getRoleName().equals(roleName))
                .findFirst();
    }
}
