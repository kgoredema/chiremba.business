package com.softhub.chiremba.business.user;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity implements UserDetails {

    private String username;
    private Set<Role> roles;
    private String fullName;
    private Boolean enabled = Boolean.TRUE;
    private Boolean accountNonExpired = Boolean.TRUE;
    private Boolean accountNonLocked = Boolean.TRUE;
    private Boolean credentialsNonExpired = Boolean.TRUE;
    private String password;
    private Boolean isActiveDirectoryUser;
    private String emailAddress;

    @Column(unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role") // Column name in user_role
    public Set<Role> getRoles() {
        return (roles == null) ? Collections.emptySet() : roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        if (roles == null) {
            return Collections.emptyList();
        }

        return roles
                .stream()
                .map(r -> new SimpleGrantedAuthority(Role.PREFIX + r.getRoleName()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean isEnabled() {
        return BooleanUtils.toBooleanDefaultIfNull(enabled, true);
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return BooleanUtils.toBooleanDefaultIfNull(accountNonExpired, true);
    }

    public void setAccountNonExpired(Boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return BooleanUtils.toBooleanDefaultIfNull(accountNonLocked, true);
    }

    public void setAccountNonLocked(Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return BooleanUtils.toBooleanDefaultIfNull(credentialsNonExpired, true);
    }

    public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public Boolean getIsActiveDirectoryUser() {
        return isActiveDirectoryUser;
    }

    public void setIsActiveDirectoryUser(Boolean activeDirectoryUser) {
        isActiveDirectoryUser = activeDirectoryUser;
    }

    @Override
    public String toString() {
        return fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
