package com.softhub.chiremba.business.user;
import com.softhub.chiremba.business.exception.ChirembaException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private  final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public User save(User user, Set<Role> newRoles) {

        final User currentUser = findCurrentUser().orElseThrow(() ->
                new ChirembaException("Cannot save user when not logged in"));

        //if (!currentUser.getId().equals(user.getId()) && !currentUser.getRoles().contains(Role.CHANGE_OTHER_USERS_DETAILS)) {
        //    throw new ChirembaException("Unauthorized to change another users' details");
       // }


        final Set<Role> existingRoles = user.getRoles();

        if (existingRoles != null) {
            existingRoles.clear();
        }

        user.setRoles(newRoles);
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) {

        if (id == null) {
            return Optional.empty();
        }
        return userRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByUserName(String username) {

        if (StringUtils.isEmpty(username)) {
            return Optional.empty();
        }

        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<User> findCurrentUser() {

        final SecurityContext context = SecurityContextHolder.getContext();

        if (context == null) {
            return Optional.empty();
        }
        final Authentication authentication = context.getAuthentication();

        if (authentication == null) {
            return Optional.empty();
        }
        final Object principal = authentication.getPrincipal();

        if (principal == null) {
            return Optional.empty();
        }

        //final UserDetails currentUserDetails = (UserDetails) principal;
       // final String currentUserName = currentUserDetails.getUsername();

        //return findByUserName(currentUserName);
        User u= new User();
        u.setPassword("System");
        return Optional.of(u);


    }

    @Override
    public void updatePassword(String password, Long userId) {
     userRepository.updatePassword(password,userId);

    }

    @Override
    public Optional<User> findByEmail(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress);
    }

    @Override
    public Optional<User> saveUserOnRegistration(User user) {
        String password= user.getPassword();
       // String $password=passwordEncoder.encode(password);
        //user.setPassword($password);
        userRepository.save(user);
        return Optional.of(user);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> savedUser = this.findByUserName(username);

        if (!savedUser.isPresent()) {
            throw new BadCredentialsException("Unable to find user with name " + username + " not found");
        }

        return savedUser.get();
    }
}
