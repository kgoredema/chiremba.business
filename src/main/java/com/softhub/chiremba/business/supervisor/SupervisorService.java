package com.softhub.chiremba.business.supervisor;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SupervisorService extends AppService<Supervisor> {

}
