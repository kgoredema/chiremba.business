package com.softhub.chiremba.business.supervisor;


import com.softhub.chiremba.business.supervisoraddress.SupervisorAddress;
import com.softhub.chiremba.business.supervisorcontact.SupervisorContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "supervisor")
public class Supervisor extends BaseEntity {

    private String firstname;
    private String surname;
    private String supervisorno;
    private String status;
    @OneToMany(mappedBy = "supervisor")
    private Set<SupervisorAddress> supervisorAddress;
    @OneToMany(mappedBy = "supervisor")
    private Set<SupervisorContact> supervisorContact;
    
}
