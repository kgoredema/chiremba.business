package com.softhub.chiremba.business.supervisor;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SupervisorServiceImpl implements SupervisorService {

    private final SupervisorRepository supervisorRepository;


    @Override
    public Optional<Supervisor> get(Object id) {
        return supervisorRepository.findById((Long) id);
    }

    @Override
    public List<Supervisor> getAll() {
        return supervisorRepository.findAll();
    }

    @Override
    public Supervisor save(Supervisor supervisor) {
        return supervisorRepository.save(supervisor);
    }

    @Override
    public void delete(Supervisor supervisor) {
        supervisorRepository.delete(supervisor);

    }
}
