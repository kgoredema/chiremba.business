package com.softhub.chiremba.business.manufactureraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerAddressRepository extends JpaRepository<ManufacturerAddress, Long> {

}
