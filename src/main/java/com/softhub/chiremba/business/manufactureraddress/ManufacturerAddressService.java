package com.softhub.chiremba.business.manufactureraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ManufacturerAddressService extends AppService<ManufacturerAddress> {

}
