package com.softhub.chiremba.business.manufactureraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ManufacturerAddressServiceImpl implements ManufacturerAddressService {

    private final ManufacturerAddressRepository manufacturerAddressRepository;


    @Override
    public Optional<ManufacturerAddress> get(Object id) {
        return manufacturerAddressRepository.findById((Long) id);
    }

    @Override
    public List<ManufacturerAddress> getAll() {
        return manufacturerAddressRepository.findAll();
    }

    @Override
    public ManufacturerAddress save(ManufacturerAddress manufacturerAddress) {
        return manufacturerAddressRepository.save(manufacturerAddress);
    }

    @Override
    public void delete(ManufacturerAddress manufacturerAddress) {
        manufacturerAddressRepository.delete(manufacturerAddress);

    }
}
