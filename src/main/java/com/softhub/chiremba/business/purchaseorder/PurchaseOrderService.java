package com.softhub.chiremba.business.purchaseorder;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PurchaseOrderService extends AppService<PurchaseOrder> {

}
