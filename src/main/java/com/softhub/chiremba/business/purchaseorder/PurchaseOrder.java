package com.softhub.chiremba.business.purchaseorder;


import com.softhub.chiremba.business.customer.Customer;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.product.Product;
import com.softhub.chiremba.business.supplier.Supplier;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Entity(name = "purchase_order")
public class PurchaseOrder extends BaseEntity {
    private String orderno;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date orderDate;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Supplier supplier;
    private double discount;
    private double netPayable;
    
}
