package com.softhub.chiremba.business.customercontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CustomerContactService extends AppService<CustomerContact> {

}
