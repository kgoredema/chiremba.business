package com.softhub.chiremba.business.customercontact;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.customer.Customer;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "clientContact")
public class CustomerContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Customer client;

}
