package com.softhub.chiremba.business.customercontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CustomerContactServiceImpl implements CustomerContactService {

    private final CustomerContactRepository clientContactRepository;


    @Override
    public Optional<CustomerContact> get(Object id) {
        return clientContactRepository.findById((Long) id);
    }

    @Override
    public List<CustomerContact> getAll() {
        return clientContactRepository.findAll();
    }

    @Override
    public CustomerContact save(CustomerContact clientContact) {
        return clientContactRepository.save(clientContact);
    }

    @Override
    public void delete(CustomerContact clientContact) {
        clientContactRepository.delete(clientContact);

    }
}
