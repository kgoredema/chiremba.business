package com.softhub.chiremba.business.title;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "title")
public class Title extends BaseEntity {

    private String name;
    private String description;

}
