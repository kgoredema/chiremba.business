package com.softhub.chiremba.business.title;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface TitleService extends AppService<Title> {

}
