package com.softhub.chiremba.business.fiscalperiod;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "fiscal_period")
public class FiscalPeriod extends BaseEntity {
    private String name;
    private String description;
    
}
