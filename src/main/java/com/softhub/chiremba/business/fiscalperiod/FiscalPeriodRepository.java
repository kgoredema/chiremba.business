package com.softhub.chiremba.business.fiscalperiod;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FiscalPeriodRepository extends JpaRepository<FiscalPeriod, Long> {

}
