package com.softhub.chiremba.business.fiscalperiod;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface FiscalPeriodService extends AppService<FiscalPeriod> {

}
