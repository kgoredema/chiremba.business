package com.softhub.chiremba.business.fiscalperiod;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class FiscalPeriodServiceImpl implements FiscalPeriodService {

    private final FiscalPeriodRepository fiscalPeriodRepository;


    @Override
    public Optional<FiscalPeriod> get(Object id) {
        return fiscalPeriodRepository.findById((Long) id);
    }

    @Override
    public List<FiscalPeriod> getAll() {
        return fiscalPeriodRepository.findAll();
    }

    @Override
    public FiscalPeriod save(FiscalPeriod fiscalPeriod) {
        return fiscalPeriodRepository.save(fiscalPeriod);
    }

    @Override
    public void delete(FiscalPeriod fiscalPeriod) {
        fiscalPeriodRepository.delete(fiscalPeriod);

    }
}
