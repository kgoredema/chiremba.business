package com.softhub.chiremba.business.doctorcontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DoctorContactService extends AppService<DoctorContact> {

}
