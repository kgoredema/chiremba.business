package com.softhub.chiremba.business.doctorcontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DoctorContactServiceImpl implements DoctorContactService {

    private final DoctorContactRepository addressTypeRepository;


    @Override
    public Optional<DoctorContact> get(Object id) {
        return addressTypeRepository.findById((Long) id);
    }

    @Override
    public List<DoctorContact> getAll() {
        return addressTypeRepository.findAll();
    }

    @Override
    public DoctorContact save(DoctorContact addressType) {
        return addressTypeRepository.save(addressType);
    }

    @Override
    public void delete(DoctorContact addressType) {
        addressTypeRepository.delete(addressType);

    }
}
