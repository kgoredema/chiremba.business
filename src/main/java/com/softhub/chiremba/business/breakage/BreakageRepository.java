package com.softhub.chiremba.business.breakage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BreakageRepository extends JpaRepository<Breakage, Long> {

}
