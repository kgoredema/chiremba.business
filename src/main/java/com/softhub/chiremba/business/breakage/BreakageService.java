package com.softhub.chiremba.business.breakage;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface BreakageService extends AppService<Breakage> {

}
