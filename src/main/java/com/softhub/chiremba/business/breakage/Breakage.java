package com.softhub.chiremba.business.breakage;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "breakage")
public class Breakage extends BaseEntity {
    
}
