package com.softhub.chiremba.business.breakage;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class BreakageServiceImpl implements BreakageService {

    private final BreakageRepository breakageRepository;


    @Override
    public Optional<Breakage> get(Object id) {
        return breakageRepository.findById((Long) id);
    }

    @Override
    public List<Breakage> getAll() {
        return breakageRepository.findAll();
    }

    @Override
    public Breakage save(Breakage breakage) {
        return breakageRepository.save(breakage);
    }

    @Override
    public void delete(Breakage breakage) {
        breakageRepository.delete(breakage);

    }
}
