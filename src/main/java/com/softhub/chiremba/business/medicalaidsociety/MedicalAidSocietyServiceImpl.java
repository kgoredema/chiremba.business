package com.softhub.chiremba.business.medicalaidsociety;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class MedicalAidSocietyServiceImpl implements MedicalAidSocietyService {

    private final MedicalAidSocietyRepository medicalAidSocietyRepository;


    @Override
    public Optional<MedicalAidSocienty> get(Object id) {
        return medicalAidSocietyRepository.findById((Long) id);
    }

    @Override
    public List<MedicalAidSocienty> getAll() {
        return medicalAidSocietyRepository.findAll();
    }

    @Override
    public MedicalAidSocienty save(MedicalAidSocienty medicalAidSociety) {
        return medicalAidSocietyRepository.save(medicalAidSociety);
    }

    @Override
    public void delete(MedicalAidSocienty medicalAidSociety) {
        medicalAidSocietyRepository.delete(medicalAidSociety);

    }
}
