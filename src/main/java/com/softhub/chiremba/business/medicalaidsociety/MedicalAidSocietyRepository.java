package com.softhub.chiremba.business.medicalaidsociety;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalAidSocietyRepository extends JpaRepository<MedicalAidSocienty, Long> {

}
