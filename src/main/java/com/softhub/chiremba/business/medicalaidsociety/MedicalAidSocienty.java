package com.softhub.chiremba.business.medicalaidsociety;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "medical_aid_society")
public class MedicalAidSocienty extends BaseEntity {
    private String name;
    private String claimcode;
    
}
