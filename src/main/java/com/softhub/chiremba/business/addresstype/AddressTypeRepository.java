package com.softhub.chiremba.business.addresstype;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressTypeRepository extends JpaRepository<AddressType, Long> {

}
