package com.softhub.chiremba.business.addresstype;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface AddressTypeService extends AppService<AddressType> {

}
