package com.softhub.chiremba.business.addresstype;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "address_type")
public class AddressType extends BaseEntity {
    private String name;
    private String description;

}
