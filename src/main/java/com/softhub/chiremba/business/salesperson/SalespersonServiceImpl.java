package com.softhub.chiremba.business.salesperson;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SalespersonServiceImpl implements SalespersonService {

    private final SalespersonRepository salespersonRepository;


    @Override
    public Optional<Salesperson> get(Object id) {
        return salespersonRepository.findById((Long) id);
    }

    @Override
    public List<Salesperson> getAll() {
        return salespersonRepository.findAll();
    }

    @Override
    public Salesperson save(Salesperson salesperson) {
        return salespersonRepository.save(salesperson);
    }

    @Override
    public void delete(Salesperson salesperson) {
        salespersonRepository.delete(salesperson);

    }
}
