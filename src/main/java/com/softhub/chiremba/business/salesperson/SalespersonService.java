package com.softhub.chiremba.business.salesperson;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SalespersonService extends AppService<Salesperson> {

}
