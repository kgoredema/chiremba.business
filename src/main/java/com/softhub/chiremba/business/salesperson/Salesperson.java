package com.softhub.chiremba.business.salesperson;

import com.softhub.chiremba.business.salespersonaddress.SalespersonAddress;
import com.softhub.chiremba.business.salespersoncontact.SalespersonContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Date;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "salesperson")
public class Salesperson extends BaseEntity {

    private String firstname;
    private String surname;
    private String salespersonno;
    private String contactPerson;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date commenceDate;
    private String status;
    @OneToMany(mappedBy = "salesperson")
    private Set<SalespersonAddress> salespersonAddress;
    @OneToMany(mappedBy = "salesperson")
    private Set<SalespersonContact> salespersonContact;

}
