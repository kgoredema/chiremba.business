package com.softhub.chiremba.business.exception;

/**
 * Created by kelvin on 2019/06/02.
 */
public class ChirembaException  extends  RuntimeException{

    public ChirembaException(String message) {
        super(message);
    }

    public ChirembaException(String message, Throwable cause) {
        super(message, cause);
    }
}
