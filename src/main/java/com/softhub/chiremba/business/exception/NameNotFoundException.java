package com.softhub.chiremba.business.exception;

/**
 * Created by kelvin on 2019/06/02.
 */
public class NameNotFoundException extends  RuntimeException{

    public NameNotFoundException(String message) {
        super(message);
    }

    public NameNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
