package com.softhub.chiremba.business.cashbook;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CashbookService extends AppService<Cashbook> {

}
