package com.softhub.chiremba.business.cashbook;

import com.softhub.chiremba.business.ledger.Ledger;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Entity(name = "cashbook")
public class Cashbook extends BaseEntity {

    private String name;
    private String bookno;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date transactionDate;
    @ManyToOne
    private Ledger account;
    private double amount;
    private double balance;

}
