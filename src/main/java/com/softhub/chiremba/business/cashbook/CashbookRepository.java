package com.softhub.chiremba.business.cashbook;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CashbookRepository extends JpaRepository<Cashbook, Long> {

}
