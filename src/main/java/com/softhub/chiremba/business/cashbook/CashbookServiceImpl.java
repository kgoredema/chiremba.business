package com.softhub.chiremba.business.cashbook;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CashbookServiceImpl implements CashbookService {

    private final CashbookRepository cashbookRepository;


    @Override
    public Optional<Cashbook> get(Object id) {
        return cashbookRepository.findById((Long) id);
    }

    @Override
    public List<Cashbook> getAll() {
        return cashbookRepository.findAll();
    }

    @Override
    public Cashbook save(Cashbook cashbook) {
        return cashbookRepository.save(cashbook);
    }

    @Override
    public void delete(Cashbook cashbook) {
        cashbookRepository.delete(cashbook);

    }
}
