package com.softhub.chiremba.business.conditions;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "conditions")
public class Conditions extends BaseEntity {

    private String name;
    private String description;

}
