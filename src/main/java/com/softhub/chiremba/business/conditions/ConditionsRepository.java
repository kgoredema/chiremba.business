package com.softhub.chiremba.business.conditions;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConditionsRepository extends JpaRepository<Conditions, Long> {

}
