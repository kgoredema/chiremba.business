package com.softhub.chiremba.business.conditions;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ConditionsService extends AppService<Conditions> {

}
