package com.softhub.chiremba.business.conditions;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ConditionsServiceImpl implements ConditionsService {

    private final ConditionsRepository conditionsRepository;


    @Override
    public Optional<Conditions> get(Object id) {
        return conditionsRepository.findById((Long) id);
    }

    @Override
    public List<Conditions> getAll() {
        return conditionsRepository.findAll();
    }

    @Override
    public Conditions save(Conditions conditions) {
        return conditionsRepository.save(conditions);
    }

    @Override
    public void delete(Conditions conditions) {
        conditionsRepository.delete(conditions);

    }
}
