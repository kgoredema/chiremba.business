package com.softhub.chiremba.business.debtor;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DebtorRepository extends JpaRepository<Debtor, Long> {

}
