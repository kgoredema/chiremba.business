package com.softhub.chiremba.business.debtor;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DebtorServiceImpl implements DebtorService {

    private final DebtorRepository debtorRepository;


    @Override
    public Optional<Debtor> get(Object id) {
        return debtorRepository.findById((Long) id);
    }

    @Override
    public List<Debtor> getAll() {
        return debtorRepository.findAll();
    }

    @Override
    public Debtor save(Debtor debtor) {
        return debtorRepository.save(debtor);
    }

    @Override
    public void delete(Debtor debtor) {
        debtorRepository.delete(debtor);

    }
}
