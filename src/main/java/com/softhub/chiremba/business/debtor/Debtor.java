package com.softhub.chiremba.business.debtor;

import com.softhub.chiremba.business.debtoraddress.DebtorAddress;
import com.softhub.chiremba.business.debtorcontact.DebtorContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Date;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "debtor")
public class Debtor extends BaseEntity {

    private String name;
    private String description;
    
    private String firstname;
    private String lastname;
    private String debtorno;
    private String contactPerson;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date commenceDate;
    private String status;
    @OneToMany(mappedBy = "debtor")
    private Set<DebtorAddress> debtorAddress;
    @OneToMany(mappedBy = "debtor")
    private Set<DebtorContact> debtorContact;

}
