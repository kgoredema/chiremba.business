package com.softhub.chiremba.business.debtor;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DebtorService extends AppService<Debtor> {

}
