package com.softhub.chiremba.business.salespersonaddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SalespersonAddressService extends AppService<SalespersonAddress> {

}
