package com.softhub.chiremba.business.salespersonaddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SalespersonAddressServiceImpl implements SalespersonAddressService {

    private final SalespersonAddressRepository salespersonAddressRepository;


    @Override
    public Optional<SalespersonAddress> get(Object id) {
        return salespersonAddressRepository.findById((Long) id);
    }

    @Override
    public List<SalespersonAddress> getAll() {
        return salespersonAddressRepository.findAll();
    }

    @Override
    public SalespersonAddress save(SalespersonAddress salespersonAddress) {
        return salespersonAddressRepository.save(salespersonAddress);
    }

    @Override
    public void delete(SalespersonAddress salespersonAddress) {
        salespersonAddressRepository.delete(salespersonAddress);

    }
}
