package com.softhub.chiremba.business.salespersonaddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SalespersonAddressRepository extends JpaRepository<SalespersonAddress, Long> {

}
