package com.softhub.chiremba.business.medicalaidscheme;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalAidSchemeRepository extends JpaRepository<MedicalAidScheme, Long> {

}
