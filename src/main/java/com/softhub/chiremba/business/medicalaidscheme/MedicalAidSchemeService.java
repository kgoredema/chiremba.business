package com.softhub.chiremba.business.medicalaidscheme;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface MedicalAidSchemeService extends AppService<MedicalAidScheme> {

}
