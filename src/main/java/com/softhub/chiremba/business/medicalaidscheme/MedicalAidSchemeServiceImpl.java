package com.softhub.chiremba.business.medicalaidscheme;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class MedicalAidSchemeServiceImpl implements MedicalAidSchemeService {

    private final MedicalAidSchemeRepository medicalAidSchemeRepository;


    @Override
    public Optional<MedicalAidScheme> get(Object id) {
        return medicalAidSchemeRepository.findById((Long) id);
    }

    @Override
    public List<MedicalAidScheme> getAll() {
        return medicalAidSchemeRepository.findAll();
    }

    @Override
    public MedicalAidScheme save(MedicalAidScheme medicalAidScheme) {
        return medicalAidSchemeRepository.save(medicalAidScheme);
    }

    @Override
    public void delete(MedicalAidScheme medicalAidScheme) {
        medicalAidSchemeRepository.delete(medicalAidScheme);

    }
}
