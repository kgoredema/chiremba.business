package com.softhub.chiremba.business.medicalaidscheme;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "medical_aid_scheme")
public class MedicalAidScheme extends BaseEntity {
    private String name;
    private String description;
    
}
