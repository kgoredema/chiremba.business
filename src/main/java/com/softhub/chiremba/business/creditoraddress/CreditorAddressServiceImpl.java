package com.softhub.chiremba.business.creditoraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CreditorAddressServiceImpl implements CreditorAddressService {

    private final CreditorAddressRepository creditorAddressRepository;


    @Override
    public Optional<CreditorAddress> get(Object id) {
        return creditorAddressRepository.findById((Long) id);
    }

    @Override
    public List<CreditorAddress> getAll() {
        return creditorAddressRepository.findAll();
    }

    @Override
    public CreditorAddress save(CreditorAddress creditorAddress) {
        return creditorAddressRepository.save(creditorAddress);
    }

    @Override
    public void delete(CreditorAddress creditorAddress) {
        creditorAddressRepository.delete(creditorAddress);

    }
}
