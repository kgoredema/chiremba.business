package com.softhub.chiremba.business.creditoraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditorAddressRepository extends JpaRepository<CreditorAddress, Long> {

}
