package com.softhub.chiremba.business.creditoraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CreditorAddressService extends AppService<CreditorAddress> {

}
