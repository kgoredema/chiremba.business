package com.softhub.chiremba.business.stocktake;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class StocktakeServiceImpl implements StocktakeService {

    private final StocktakeRepository stocktakeRepository;


    @Override
    public Optional<Stocktake> get(Object id) {
        return stocktakeRepository.findById((Long) id);
    }

    @Override
    public List<Stocktake> getAll() {
        return stocktakeRepository.findAll();
    }

    @Override
    public Stocktake save(Stocktake stocktake) {
        return stocktakeRepository.save(stocktake);
    }

    @Override
    public void delete(Stocktake stocktake) {
        stocktakeRepository.delete(stocktake);

    }
}
