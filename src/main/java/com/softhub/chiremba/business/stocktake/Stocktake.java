package com.softhub.chiremba.business.stocktake;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "stocktake")
public class Stocktake extends BaseEntity {
    private String name;
    private String description;
    
}
