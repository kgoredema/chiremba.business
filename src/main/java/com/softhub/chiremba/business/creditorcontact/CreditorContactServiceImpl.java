package com.softhub.chiremba.business.creditorcontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CreditorContactServiceImpl implements CreditorContactService {

    private final CreditorContactRepository creditorContactRepository;


    @Override
    public Optional<CreditorContact> get(Object id) {
        return creditorContactRepository.findById((Long) id);
    }

    @Override
    public List<CreditorContact> getAll() {
        return creditorContactRepository.findAll();
    }

    @Override
    public CreditorContact save(CreditorContact creditorContact) {
        return creditorContactRepository.save(creditorContact);
    }

    @Override
    public void delete(CreditorContact creditorContact) {
        creditorContactRepository.delete(creditorContact);

    }
}
