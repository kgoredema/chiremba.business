package com.softhub.chiremba.business.creditorcontact;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.creditor.Creditor;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "creditor_contact")
public class CreditorContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Creditor creditor;

}
