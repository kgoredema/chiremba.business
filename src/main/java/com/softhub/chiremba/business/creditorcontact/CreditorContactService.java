package com.softhub.chiremba.business.creditorcontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CreditorContactService extends AppService<CreditorContact> {

}
