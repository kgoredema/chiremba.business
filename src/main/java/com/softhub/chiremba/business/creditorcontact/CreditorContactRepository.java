package com.softhub.chiremba.business.creditorcontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditorContactRepository extends JpaRepository<CreditorContact, Long> {

}
