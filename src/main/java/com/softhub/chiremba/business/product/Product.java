package com.softhub.chiremba.business.product;

import com.softhub.chiremba.business.barcode.Barcode;
import com.softhub.chiremba.business.model.AuditedEntity;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by kelvin on 2019/05/31.
 */
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "product")
@Data
public class Product extends AuditedEntity {

    @Id
    @Column(name = "code")
    private  String code;
    @Column(name = "name")
    private  String name;
    @Column(name = "description")
    private  String description;
    @Column(name = "selling_price")
    private BigDecimal sellingPrice;

    private int version;
    @OneToMany(mappedBy="product")
    private Set<Barcode> barcodes;




}
