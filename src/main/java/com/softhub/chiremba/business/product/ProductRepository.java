package com.softhub.chiremba.business.product;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kelvin on 2019/05/31.
 */
public interface ProductRepository extends JpaRepository<Product,String> {
}
