package com.softhub.chiremba.business.product;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by kelvin on 2019/05/31.
 */

public interface ProductService extends AppService<Product> {
}
