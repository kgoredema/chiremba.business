package com.softhub.chiremba.business.pricestructure;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PriceStructureService extends AppService<PriceStructure> {

}
