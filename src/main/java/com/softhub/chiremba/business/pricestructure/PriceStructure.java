package com.softhub.chiremba.business.pricestructure;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "price_structure")
public class PriceStructure extends BaseEntity {
    private String name;
    private String description;
    
}
