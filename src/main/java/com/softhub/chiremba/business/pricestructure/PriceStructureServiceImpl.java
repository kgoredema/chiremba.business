package com.softhub.chiremba.business.pricestructure;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class PriceStructureServiceImpl implements PriceStructureService {

    private final PriceStructureRepository priceStructureRepository;


    @Override
    public Optional<PriceStructure> get(Object id) {
        return priceStructureRepository.findById((Long) id);
    }

    @Override
    public List<PriceStructure> getAll() {
        return priceStructureRepository.findAll();
    }

    @Override
    public PriceStructure save(PriceStructure priceStructure) {
        return priceStructureRepository.save(priceStructure);
    }

    @Override
    public void delete(PriceStructure priceStructure) {
        priceStructureRepository.delete(priceStructure);

    }
}
