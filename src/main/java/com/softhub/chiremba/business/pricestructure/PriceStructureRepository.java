package com.softhub.chiremba.business.pricestructure;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceStructureRepository extends JpaRepository<PriceStructure, Long> {

}
