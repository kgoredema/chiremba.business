package com.softhub.chiremba.business.patient;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PatientService extends AppService<Patient> {

}
