package com.softhub.chiremba.business.barcode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by kelvin on 2019/06/13.
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class BarcodeServiceImpl  implements  BarcodeService{

    private final BarcodeRepository repository;
    @Override
    public Optional<Barcode> get(Object id) {
        return repository.findById((Long)id);
    }

    @Override
    public List<Barcode> getAll() {
        return repository.findAll();
    }

    @Override
    public Barcode save(Barcode barcode) {
        return repository.save(barcode);
    }

    @Override
    public void delete(Barcode barcode) {
       repository.delete(barcode);
    }
}
