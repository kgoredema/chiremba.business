package com.softhub.chiremba.business.barcode;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by kelvin on 2019/06/13.
 */
public  interface BarcodeService  extends AppService<Barcode>{
}
