package com.softhub.chiremba.business.barcode;

import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.product.Product;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kelvin on 2019/06/13.
 */

@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "barcode")
@Data
public class Barcode extends BaseEntity{

    @Column(name = "barcode",unique = true)
    private Long barcode;

    @ManyToOne
    @JoinColumn(name="product_code", nullable=false)
    private Product product;

    @Column(name = "unit", length = 10)
    private String unit;
    @Column(name = "unit_size", length = 5)
    private BigDecimal unitSize;



}
