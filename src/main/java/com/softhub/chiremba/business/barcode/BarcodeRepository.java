package com.softhub.chiremba.business.barcode;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kelvin on 2019/06/13.
 */
public interface BarcodeRepository extends JpaRepository<Barcode,Long>{
}
