package com.softhub.chiremba.business.quotationItemitem;


import com.softhub.chiremba.business.quotationitem.QuotationItem;
import com.softhub.chiremba.business.quotationitem.QuotationItemRepository;
import com.softhub.chiremba.business.quotationitem.QuotationItemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class QuotationItemServiceImpl implements QuotationItemService {

    private final QuotationItemRepository quotationItemRepository;


    @Override
    public Optional<QuotationItem> get(Object id) {
        return quotationItemRepository.findById((Long) id);
    }

    @Override
    public List<QuotationItem> getAll() {
        return quotationItemRepository.findAll();
    }

    @Override
    public QuotationItem save(QuotationItem quotationItem) {
        return quotationItemRepository.save(quotationItem);
    }

    @Override
    public void delete(QuotationItem quotationItem) {
        quotationItemRepository.delete(quotationItem);

    }
}
