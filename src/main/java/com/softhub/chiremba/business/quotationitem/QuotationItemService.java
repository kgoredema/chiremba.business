package com.softhub.chiremba.business.quotationitem;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface QuotationItemService extends AppService<QuotationItem> {

}
