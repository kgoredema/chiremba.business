package com.softhub.chiremba.business.quotationitem;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "quotation_item")
public class QuotationItem extends BaseEntity {
    
    private String item;
    private int quantity;
    private double price;
    private double vat;
    private double total;
    
    
}
