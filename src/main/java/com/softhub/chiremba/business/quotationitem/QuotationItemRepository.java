package com.softhub.chiremba.business.quotationitem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuotationItemRepository extends JpaRepository<QuotationItem, Long> {

}
