package com.softhub.chiremba.business.exchangerate;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ExchangeRateService extends AppService<ExchangeRate> {

}
