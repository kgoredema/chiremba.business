package com.softhub.chiremba.business.exchangerate;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final ExchangeRateRepository exchangeRateRepository;


    @Override
    public Optional<ExchangeRate> get(Object id) {
        return exchangeRateRepository.findById((Long) id);
    }

    @Override
    public List<ExchangeRate> getAll() {
        return exchangeRateRepository.findAll();
    }

    @Override
    public ExchangeRate save(ExchangeRate exchangeRate) {
        return exchangeRateRepository.save(exchangeRate);
    }

    @Override
    public void delete(ExchangeRate exchangeRate) {
        exchangeRateRepository.delete(exchangeRate);

    }
}
