package com.softhub.chiremba.business.exchangerate;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "exchange_rate")
public class ExchangeRate extends BaseEntity {
    private String name;
    private String description;
    
}
