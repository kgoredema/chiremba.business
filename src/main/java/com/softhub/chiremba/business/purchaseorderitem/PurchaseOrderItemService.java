package com.softhub.chiremba.business.purchaseorderitem;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PurchaseOrderItemService extends AppService<PurchaseOrderItem> {

}
