package com.softhub.chiremba.business.purchaseorderitem;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "purchase_order_item")
public class PurchaseOrderItem extends BaseEntity {

    private String item;
    private int quantity;
    private double price;
    private double vat;
    private double total;

}
