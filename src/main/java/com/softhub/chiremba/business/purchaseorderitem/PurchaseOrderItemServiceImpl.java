package com.softhub.chiremba.business.purchaseorderitem;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class PurchaseOrderItemServiceImpl implements PurchaseOrderItemService {

    private final PurchaseOrderItemRepository purchaseOrderItemRepository;


    @Override
    public Optional<PurchaseOrderItem> get(Object id) {
        return purchaseOrderItemRepository.findById((Long) id);
    }

    @Override
    public List<PurchaseOrderItem> getAll() {
        return purchaseOrderItemRepository.findAll();
    }

    @Override
    public PurchaseOrderItem save(PurchaseOrderItem purchaseOrderItem) {
        return purchaseOrderItemRepository.save(purchaseOrderItem);
    }

    @Override
    public void delete(PurchaseOrderItem purchaseOrderItem) {
        purchaseOrderItemRepository.delete(purchaseOrderItem);

    }
}
