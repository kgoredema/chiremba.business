package com.softhub.chiremba.business.purchaseorderitem;

import com.softhub.chiremba.business.quotation.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItem, Long> {

}
