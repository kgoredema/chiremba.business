package com.softhub.chiremba.business.suppliercontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierContactRepository extends JpaRepository<SupplierContact, Long> {

}
