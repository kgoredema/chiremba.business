package com.softhub.chiremba.business.suppliercontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SupplierContactService extends AppService<SupplierContact> {

}
