package com.softhub.chiremba.business.suppliercontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SupplierContactServiceImpl implements SupplierContactService {

    private final SupplierContactRepository supplierContactRepository;


    @Override
    public Optional<SupplierContact> get(Object id) {
        return supplierContactRepository.findById((Long) id);
    }

    @Override
    public List<SupplierContact> getAll() {
        return supplierContactRepository.findAll();
    }

    @Override
    public SupplierContact save(SupplierContact supplierContact) {
        return supplierContactRepository.save(supplierContact);
    }

    @Override
    public void delete(SupplierContact supplierContact) {
        supplierContactRepository.delete(supplierContact);

    }
}
