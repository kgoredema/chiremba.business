package com.softhub.chiremba.business.salespersoncontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SalespersonContactRepository extends JpaRepository<SalespersonContact, Long> {

}
