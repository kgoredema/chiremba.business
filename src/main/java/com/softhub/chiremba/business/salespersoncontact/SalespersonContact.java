package com.softhub.chiremba.business.salespersoncontact;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.salesperson.Salesperson;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "Salesperson_contact")
public class SalespersonContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Salesperson salesperson;

}
