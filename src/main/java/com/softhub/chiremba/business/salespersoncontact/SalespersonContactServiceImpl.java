package com.softhub.chiremba.business.salespersoncontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SalespersonContactServiceImpl implements SalespersonContactService {

    private final SalespersonContactRepository salespersonContactRepository;


    @Override
    public Optional<SalespersonContact> get(Object id) {
        return salespersonContactRepository.findById((Long) id);
    }

    @Override
    public List<SalespersonContact> getAll() {
        return salespersonContactRepository.findAll();
    }

    @Override
    public SalespersonContact save(SalespersonContact salespersonContact) {
        return salespersonContactRepository.save(salespersonContact);
    }

    @Override
    public void delete(SalespersonContact salespersonContact) {
        salespersonContactRepository.delete(salespersonContact);

    }
}
