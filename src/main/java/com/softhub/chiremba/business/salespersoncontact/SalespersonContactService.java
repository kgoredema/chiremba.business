package com.softhub.chiremba.business.salespersoncontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SalespersonContactService extends AppService<SalespersonContact> {

}
