package com.softhub.chiremba.business.pharmacist;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class PharmacistServiceImpl implements PharmacistService {

    private final PharmacistRepository pharmacistRepository;


    @Override
    public Optional<Pharmacist> get(Object id) {
        return pharmacistRepository.findById((Long) id);
    }

    @Override
    public List<Pharmacist> getAll() {
        return pharmacistRepository.findAll();
    }

    @Override
    public Pharmacist save(Pharmacist pharmacist) {
        return pharmacistRepository.save(pharmacist);
    }

    @Override
    public void delete(Pharmacist pharmacist) {
        pharmacistRepository.delete(pharmacist);

    }
}
