package com.softhub.chiremba.business.pharmacist;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "pharmacist")
public class Pharmacist extends BaseEntity {
    private String name;
    private String description;
    
}
