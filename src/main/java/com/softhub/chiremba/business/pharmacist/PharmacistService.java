package com.softhub.chiremba.business.pharmacist;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PharmacistService extends AppService<Pharmacist> {

}
