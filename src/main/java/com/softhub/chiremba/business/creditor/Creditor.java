package com.softhub.chiremba.business.creditor;

import com.softhub.chiremba.business.creditoraddress.CreditorAddress;
import com.softhub.chiremba.business.creditorcontact.CreditorContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Date;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "creditor")
public class Creditor extends BaseEntity {

    private String firstname;
    private String surname;
    private String idno;
    private String contactPerson;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date commenceDate;
    private String status;
    @OneToMany(mappedBy = "creditor")
    private Set<CreditorAddress> creditorAddress;
    @OneToMany(mappedBy = "creditor")
    private Set<CreditorContact> creditorContact;

}
