package com.softhub.chiremba.business.creditor;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditorRepository extends JpaRepository<Creditor, Long> {

}
