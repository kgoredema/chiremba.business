package com.softhub.chiremba.business.creditor;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CreditorService extends AppService<Creditor> {

}
