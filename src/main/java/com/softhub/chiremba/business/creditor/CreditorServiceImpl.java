package com.softhub.chiremba.business.creditor;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CreditorServiceImpl implements CreditorService {

    private final CreditorRepository creditorRepository;


    @Override
    public Optional<Creditor> get(Object id) {
        return creditorRepository.findById((Long) id);
    }

    @Override
    public List<Creditor> getAll() {
        return creditorRepository.findAll();
    }

    @Override
    public Creditor save(Creditor creditor) {
        return creditorRepository.save(creditor);
    }

    @Override
    public void delete(Creditor creditor) {
        creditorRepository.delete(creditor);

    }
}
