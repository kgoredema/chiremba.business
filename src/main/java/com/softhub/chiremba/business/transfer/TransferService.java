package com.softhub.chiremba.business.transfer;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface TransferService extends AppService<Transfer> {

}
