package com.softhub.chiremba.business.transfer;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "transfer")
public class Transfer extends BaseEntity {
    private String name;
    private String description;
    
}
