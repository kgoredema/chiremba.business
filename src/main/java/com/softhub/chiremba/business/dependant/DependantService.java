package com.softhub.chiremba.business.dependant;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DependantService extends AppService<Dependant> {

}
