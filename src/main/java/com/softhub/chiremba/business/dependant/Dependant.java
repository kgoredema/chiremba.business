package com.softhub.chiremba.business.dependant;


import com.softhub.chiremba.business.allergy.Allergy;
import com.softhub.chiremba.business.conditions.Conditions;
import com.softhub.chiremba.business.doctor.Doctor;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.title.Title;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Entity(name = "dependant")
public class Dependant extends BaseEntity {
    private String firstname;
    private String surname;
    private String middlename;
    @ManyToOne
    private Title title;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthdate;
    @ManyToOne
    private Doctor doctor;
    @ManyToOne
    private Conditions conditions;
    @ManyToOne
    private Allergy allergy;
    private String phone;
    
}
