package com.softhub.chiremba.business.dependant;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DependantRepository extends JpaRepository<Dependant, Long> {

}
