package com.softhub.chiremba.business.dependant;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DependantServiceImpl implements DependantService {

    private final DependantRepository dependantRepository;


    @Override
    public Optional<Dependant> get(Object id) {
        return dependantRepository.findById((Long) id);
    }

    @Override
    public List<Dependant> getAll() {
        return dependantRepository.findAll();
    }

    @Override
    public Dependant save(Dependant dependant) {
        return dependantRepository.save(dependant);
    }

    @Override
    public void delete(Dependant dependant) {
        dependantRepository.delete(dependant);

    }
}
