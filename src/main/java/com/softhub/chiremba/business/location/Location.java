package com.softhub.chiremba.business.location;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "location")
public class Location extends BaseEntity {
    private String name;
    private String description;
    
}
