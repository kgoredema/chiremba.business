package com.softhub.chiremba.business.location;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface LocationService extends AppService<Location> {

}
