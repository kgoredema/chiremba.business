package com.softhub.chiremba.business.supplieraddress;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.supplier.Supplier;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "supplier_address")
public class SupplierAddress extends BaseEntity {

    private String contactDetail;
    private boolean active;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Supplier supplier;

}
