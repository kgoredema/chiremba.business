package com.softhub.chiremba.business.supplieraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SupplierAddressService extends AppService<SupplierAddress> {

}
