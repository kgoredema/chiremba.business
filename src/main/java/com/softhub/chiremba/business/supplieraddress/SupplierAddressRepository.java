package com.softhub.chiremba.business.supplieraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierAddressRepository extends JpaRepository<SupplierAddress, Long> {

}
