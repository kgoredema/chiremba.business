package com.softhub.chiremba.business.supplieraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SupplierAddressServiceImpl implements SupplierAddressService {

    private final SupplierAddressRepository supplierAddressRepository;


    @Override
    public Optional<SupplierAddress> get(Object id) {
        return supplierAddressRepository.findById((Long) id);
    }

    @Override
    public List<SupplierAddress> getAll() {
        return supplierAddressRepository.findAll();
    }

    @Override
    public SupplierAddress save(SupplierAddress supplierAddress) {
        return supplierAddressRepository.save(supplierAddress);
    }

    @Override
    public void delete(SupplierAddress supplierAddress) {
        supplierAddressRepository.delete(supplierAddress);

    }
}
