package com.softhub.chiremba.business.allergy;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "allergy")
public class Allergy extends BaseEntity {

    private String name;
    private String description;

}
