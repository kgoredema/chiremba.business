package com.softhub.chiremba.business.allergy;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class AllergyServiceImpl implements AllergyService {

    private final AllergyRepository allergyRepository;


    @Override
    public Optional<Allergy> get(Object id) {
        return allergyRepository.findById((Long) id);
    }

    @Override
    public List<Allergy> getAll() {
        return allergyRepository.findAll();
    }

    @Override
    public Allergy save(Allergy allergy) {
        return allergyRepository.save(allergy);
    }

    @Override
    public void delete(Allergy allergy) {
        allergyRepository.delete(allergy);

    }
}
