package com.softhub.chiremba.business.allergy;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface AllergyService extends AppService<Allergy> {

}
