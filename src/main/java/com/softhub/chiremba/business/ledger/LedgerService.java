package com.softhub.chiremba.business.ledger;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface LedgerService extends AppService<Ledger> {

}
