package com.softhub.chiremba.business.ledger;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "ledger")
public class Ledger extends BaseEntity {
    private String name;
    private String description;
    
}
