package com.softhub.chiremba.business.ledger;


import com.softhub.chiremba.business.addresstype.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class LedgerServiceImpl implements LedgerService {

    private final LedgerRepository ledgerRepository;


    @Override
    public Optional<Ledger> get(Object id) {
        return ledgerRepository.findById((Long) id);
    }

    @Override
    public List<Ledger> getAll() {
        return ledgerRepository.findAll();
    }

    @Override
    public Ledger save(Ledger ledger) {
        return ledgerRepository.save(ledger);
    }

    @Override
    public void delete(Ledger ledger) {
        ledgerRepository.delete(ledger);

    }
}
