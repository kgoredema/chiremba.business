package com.softhub.chiremba.business.doctor;

import com.softhub.chiremba.business.doctoraddress.DoctorAddress;
import com.softhub.chiremba.business.doctorcontact.DoctorContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Date;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "doctor")
public class Doctor extends BaseEntity {
    
    private String firstname;
    private String lastname;
    private String doctorno;
    private String contactPerson;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date commenceDate;
    private String status;
    @OneToMany(mappedBy = "doctor")
    private Set<DoctorAddress> doctorAddress;
    @OneToMany(mappedBy = "doctor")
    private Set<DoctorContact> doctorContact;


}
