package com.softhub.chiremba.business.doctor;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DoctorService extends AppService<Doctor> {

}
