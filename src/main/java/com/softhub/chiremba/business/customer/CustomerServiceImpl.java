package com.softhub.chiremba.business.customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository clientRepository;

    @Override
    public Optional<Customer> get(Object id) {
        return clientRepository.findById((Long) id);
    }

    @Override
    public List<Customer> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public Customer save(Customer client) {
        return clientRepository.save(client);
    }

    @Override
    public void delete(Customer client) {
        clientRepository.delete(client);

    }
}
