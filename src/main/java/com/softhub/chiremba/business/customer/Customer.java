package com.softhub.chiremba.business.customer;


import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.customeraddress.CustomerAddress;
import com.softhub.chiremba.business.customercontact.CustomerContact;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "Customer")
public class Customer extends BaseEntity {
    private String firstname;
    private String surname;
    private String clientno;
    private String status;
    @OneToMany(mappedBy = "client")
    private Set<CustomerAddress> clientAddress;
    @OneToMany(mappedBy = "client")
    private Set<CustomerContact> clientContact;

}
