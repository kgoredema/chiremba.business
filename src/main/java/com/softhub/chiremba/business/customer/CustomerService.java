package com.softhub.chiremba.business.customer;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by kelvin on 2019/05/31.
 */
public interface CustomerService extends AppService<Customer> {
}
