package com.softhub.chiremba.business.stock;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface StockService extends AppService<Stock> {

}
