package com.softhub.chiremba.business.stock;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "stock")
public class Stock extends BaseEntity {
    private String name;
    private String description;
    
}
