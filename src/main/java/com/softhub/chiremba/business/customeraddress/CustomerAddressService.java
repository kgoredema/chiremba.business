package com.softhub.chiremba.business.customeraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CustomerAddressService extends AppService<CustomerAddress> {

}
