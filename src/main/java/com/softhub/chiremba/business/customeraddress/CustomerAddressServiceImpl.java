package com.softhub.chiremba.business.customeraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CustomerAddressServiceImpl implements CustomerAddressService {

    private final CustomerAddressRepository clientAddressRepository;


    @Override
    public Optional<CustomerAddress> get(Object id) {
        return clientAddressRepository.findById((Long) id);
    }

    @Override
    public List<CustomerAddress> getAll() {
        return clientAddressRepository.findAll();
    }

    @Override
    public CustomerAddress save(CustomerAddress clientAddress) {
        return clientAddressRepository.save(clientAddress);
    }

    @Override
    public void delete(CustomerAddress clientAddress) {
        clientAddressRepository.delete(clientAddress);

    }
}
