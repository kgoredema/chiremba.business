package com.softhub.chiremba.business.bank;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class BankServiceImpl implements BankService {

    private final BankRepository bankRepository;


    @Override
    public Optional<Bank> get(Object id) {
        return bankRepository.findById((Long) id);
    }

    @Override
    public List<Bank> getAll() {
        return bankRepository.findAll();
    }

    @Override
    public Bank save(Bank bank) {
        return bankRepository.save(bank);
    }

    @Override
    public void delete(Bank bank) {
        bankRepository.delete(bank);

    }
}
