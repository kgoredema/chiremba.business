package com.softhub.chiremba.business.bank;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface BankService extends AppService<Bank> {

}
