package com.softhub.chiremba.business.bank;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "bank")
public class Bank extends BaseEntity {

    private String bank;
    private String branch;
    private Integer accountCode;
    private String accountName;
    private Integer accountno;

}
