package com.softhub.chiremba.business.manufacturer;


import com.softhub.chiremba.business.manufactureraddress.ManufacturerAddress;
import com.softhub.chiremba.business.manufacturercontact.ManufacturerContact;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(name = "manufacturer")
public class Manufacturer extends BaseEntity {   
    private String firstname;
    private String surname;
    private String manufacturerno;
    private String status;
    @OneToMany(mappedBy = "manufacturer")
    private Set<ManufacturerAddress> manufacturerAddress =new HashSet<>();
    @OneToMany(mappedBy = "manufacturer")
    private Set<ManufacturerContact> manufacturerContact = new  HashSet<>();
    
}
