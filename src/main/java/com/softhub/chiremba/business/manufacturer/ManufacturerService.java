package com.softhub.chiremba.business.manufacturer;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ManufacturerService extends AppService<Manufacturer> {

}
