package com.softhub.chiremba.business.cashier;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CashierServiceImpl implements CashierService {

    private final CashierRepository cashierRepository;


    @Override
    public Optional<Cashier> get(Object id) {
        return cashierRepository.findById((Long) id);
    }

    @Override
    public List<Cashier> getAll() {
        return cashierRepository.findAll();
    }

    @Override
    public Cashier save(Cashier cashier) {
        return cashierRepository.save(cashier);
    }

    @Override
    public void delete(Cashier cashier) {
        cashierRepository.delete(cashier);

    }
}
