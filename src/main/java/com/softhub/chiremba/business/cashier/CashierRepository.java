package com.softhub.chiremba.business.cashier;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CashierRepository extends JpaRepository<Cashier, Long> {

}
