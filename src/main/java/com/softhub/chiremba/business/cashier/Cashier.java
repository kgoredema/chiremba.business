package com.softhub.chiremba.business.cashier;


import com.softhub.chiremba.business.cashiercontact.CashierContact;
import com.softhub.chiremba.business.cashieraddress.CashierAddress;
import com.softhub.chiremba.business.model.BaseEntity;
import java.util.Set;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Getter
@Setter
@Entity(name = "cashier")
@Builder
@Access(AccessType.FIELD)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Cashier extends BaseEntity {
    private String name;
    private String lastname;
    private String fullName;
    private String  code;
    @OneToMany(mappedBy = "cashier")
    private Set<CashierAddress> cashierAddress;
    @OneToMany(mappedBy = "cashier")
    private Set<CashierContact> cashierContact;
}
