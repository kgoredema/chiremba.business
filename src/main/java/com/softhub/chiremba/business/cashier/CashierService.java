package com.softhub.chiremba.business.cashier;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CashierService extends AppService<Cashier> {

}
