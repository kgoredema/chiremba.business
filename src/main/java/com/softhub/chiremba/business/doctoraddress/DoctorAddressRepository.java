package com.softhub.chiremba.business.doctoraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorAddressRepository extends JpaRepository<DoctorAddress, Long> {

}
