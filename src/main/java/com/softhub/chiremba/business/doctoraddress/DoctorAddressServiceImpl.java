package com.softhub.chiremba.business.doctoraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DoctorAddressServiceImpl implements DoctorAddressService {

    private final DoctorAddressRepository doctorAddressRepository;


    @Override
    public Optional<DoctorAddress> get(Object id) {
        return doctorAddressRepository.findById((Long) id);
    }

    @Override
    public List<DoctorAddress> getAll() {
        return doctorAddressRepository.findAll();
    }

    @Override
    public DoctorAddress save(DoctorAddress doctorAddress) {
        return doctorAddressRepository.save(doctorAddress);
    }

    @Override
    public void delete(DoctorAddress doctorAddress) {
        doctorAddressRepository.delete(doctorAddress);

    }
}
