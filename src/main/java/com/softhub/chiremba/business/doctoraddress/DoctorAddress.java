package com.softhub.chiremba.business.doctoraddress;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.doctor.Doctor;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "doctor_address")
public class DoctorAddress extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Doctor doctor;

}
