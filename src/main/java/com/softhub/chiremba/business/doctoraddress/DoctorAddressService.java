package com.softhub.chiremba.business.doctoraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DoctorAddressService extends AppService<DoctorAddress> {

}
