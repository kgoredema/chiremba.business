package com.softhub.chiremba.business.debtoraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DebtorAddressServiceImpl implements DebtorAddressService {

    private final DebtorAddressRepository debtorAddressRepository;


    @Override
    public Optional<DebtorAddress> get(Object id) {
        return debtorAddressRepository.findById((Long) id);
    }

    @Override
    public List<DebtorAddress> getAll() {
        return debtorAddressRepository.findAll();
    }

    @Override
    public DebtorAddress save(DebtorAddress debtorAddress) {
        return debtorAddressRepository.save(debtorAddress);
    }

    @Override
    public void delete(DebtorAddress debtorAddress) {
        debtorAddressRepository.delete(debtorAddress);

    }
}
