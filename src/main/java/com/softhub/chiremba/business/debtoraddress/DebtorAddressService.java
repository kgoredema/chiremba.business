package com.softhub.chiremba.business.debtoraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DebtorAddressService extends AppService<DebtorAddress> {

}
