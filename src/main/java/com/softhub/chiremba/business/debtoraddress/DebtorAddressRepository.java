package com.softhub.chiremba.business.debtoraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DebtorAddressRepository extends JpaRepository<DebtorAddress, Long> {

}
