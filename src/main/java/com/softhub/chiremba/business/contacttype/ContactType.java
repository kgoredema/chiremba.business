package com.softhub.chiremba.business.contacttype;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "contact_type")
public class ContactType extends BaseEntity {

    private String name;
    private String description;

}
