package com.softhub.chiremba.business.contacttype;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ContactTypeService extends AppService<ContactType> {

}
