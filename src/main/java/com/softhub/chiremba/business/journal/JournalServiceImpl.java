package com.softhub.chiremba.business.journal;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class JournalServiceImpl implements JournalService {

    private final JournalRepository journalRepository;


    @Override
    public Optional<Journal> get(Object id) {
        return journalRepository.findById((Long) id);
    }

    @Override
    public List<Journal> getAll() {
        return journalRepository.findAll();
    }

    @Override
    public Journal save(Journal journal) {
        return journalRepository.save(journal);
    }

    @Override
    public void delete(Journal journal) {
        journalRepository.delete(journal);

    }
}
