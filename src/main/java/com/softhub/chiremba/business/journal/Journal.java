package com.softhub.chiremba.business.journal;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "journal")
public class Journal extends BaseEntity {
    private String name;
    private String description;
    
}
