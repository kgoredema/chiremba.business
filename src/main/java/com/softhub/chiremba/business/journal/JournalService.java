package com.softhub.chiremba.business.journal;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface JournalService extends AppService<Journal> {

}
