package com.softhub.chiremba.business.cashieraddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CashierAddressRepository extends JpaRepository<CashierAddress, Long> {

}
