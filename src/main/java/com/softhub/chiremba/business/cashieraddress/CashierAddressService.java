package com.softhub.chiremba.business.cashieraddress;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CashierAddressService extends AppService<CashierAddress> {

}
