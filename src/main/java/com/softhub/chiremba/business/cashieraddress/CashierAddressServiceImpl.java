package com.softhub.chiremba.business.cashieraddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CashierAddressServiceImpl implements CashierAddressService {

    private final CashierAddressRepository cashierAddressRepository;


    @Override
    public Optional<CashierAddress> get(Object id) {
        return cashierAddressRepository.findById((Long) id);
    }

    @Override
    public List<CashierAddress> getAll() {
        return cashierAddressRepository.findAll();
    }

    @Override
    public CashierAddress save(CashierAddress cashierAddress) {
        return cashierAddressRepository.save(cashierAddress);
    }

    @Override
    public void delete(CashierAddress cashierAddress) {
        cashierAddressRepository.delete(cashierAddress);

    }
}
