package com.softhub.chiremba.business.cashieraddress;

import com.softhub.chiremba.business.cashier.Cashier;
import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "cashier_address")
public class CashierAddress extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Cashier cashier;

}
