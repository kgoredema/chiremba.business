package com.softhub.chiremba.business.quotation;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface QuotationService extends AppService<Quotation> {

}
