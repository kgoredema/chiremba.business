package com.softhub.chiremba.business.quotation;


import com.softhub.chiremba.business.customer.Customer;
import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.product.Product;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Entity(name = "quotation")
public class Quotation extends BaseEntity {
    private String quotationno;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date quoteDate;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Customer customer;
    private double discount;
    private double netPayable;
    
    
}
