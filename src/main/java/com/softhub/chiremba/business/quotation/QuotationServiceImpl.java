package com.softhub.chiremba.business.quotation;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class QuotationServiceImpl implements QuotationService {

    private final QuotationRepository quotationRepository;


    @Override
    public Optional<Quotation> get(Object id) {
        return quotationRepository.findById((Long) id);
    }

    @Override
    public List<Quotation> getAll() {
        return quotationRepository.findAll();
    }

    @Override
    public Quotation save(Quotation quotation) {
        return quotationRepository.save(quotation);
    }

    @Override
    public void delete(Quotation quotation) {
        quotationRepository.delete(quotation);

    }
}
