package com.softhub.chiremba.business.claim;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ClaimService extends AppService<Claim> {

}
