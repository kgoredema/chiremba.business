package com.softhub.chiremba.business.claim;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "claim")
public class Claim extends BaseEntity {
    private String name;
    private String description;
    
}
