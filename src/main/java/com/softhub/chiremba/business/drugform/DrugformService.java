package com.softhub.chiremba.business.drugform;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DrugformService extends AppService<Drugform> {

}
