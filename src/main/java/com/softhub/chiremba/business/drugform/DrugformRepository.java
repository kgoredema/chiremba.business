package com.softhub.chiremba.business.drugform;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugformRepository extends JpaRepository<Drugform, Long> {

}
