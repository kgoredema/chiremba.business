package com.softhub.chiremba.business.drugform;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DrugformServiceImpl implements DrugformService {

    private final DrugformRepository drugformRepository;


    @Override
    public Optional<Drugform> get(Object id) {
        return drugformRepository.findById((Long) id);
    }

    @Override
    public List<Drugform> getAll() {
        return drugformRepository.findAll();
    }

    @Override
    public Drugform save(Drugform drugform) {
        return drugformRepository.save(drugform);
    }

    @Override
    public void delete(Drugform drugform) {
        drugformRepository.delete(drugform);

    }
}
