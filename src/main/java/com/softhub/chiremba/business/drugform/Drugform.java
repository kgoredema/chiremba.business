package com.softhub.chiremba.business.drugform;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "drugform")
public class Drugform extends BaseEntity {
    private String name;
    private String description;
    
}
