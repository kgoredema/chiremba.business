package com.softhub.chiremba.business.cashiercontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CashierContactService extends AppService<CashierContact> {

}
