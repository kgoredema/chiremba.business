package com.softhub.chiremba.business.cashiercontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CashierContactServiceImpl implements CashierContactService {

    private final CashierContactRepository cashierContactRepository;


    @Override
    public Optional<CashierContact> get(Object id) {
        return cashierContactRepository.findById((Long) id);
    }

    @Override
    public List<CashierContact> getAll() {
        return cashierContactRepository.findAll();
    }

    @Override
    public CashierContact save(CashierContact cashierContact) {
        return cashierContactRepository.save(cashierContact);
    }

    @Override
    public void delete(CashierContact cashierContact) {
        cashierContactRepository.delete(cashierContact);

    }
}
