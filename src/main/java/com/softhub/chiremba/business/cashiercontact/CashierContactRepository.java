package com.softhub.chiremba.business.cashiercontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CashierContactRepository extends JpaRepository<CashierContact, Long> {

}
