package com.softhub.chiremba.business.cashiercontact;

import com.softhub.chiremba.business.model.BaseEntity;
import com.softhub.chiremba.business.cashier.Cashier;
import com.softhub.chiremba.business.contacttype.ContactType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "cashier_contact")
public class CashierContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Cashier cashier;

}
