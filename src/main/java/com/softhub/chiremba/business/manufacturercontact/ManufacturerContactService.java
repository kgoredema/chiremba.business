package com.softhub.chiremba.business.manufacturercontact;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ManufacturerContactService extends AppService<ManufacturerContact> {

}
