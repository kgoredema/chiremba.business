package com.softhub.chiremba.business.manufacturercontact;

import com.softhub.chiremba.business.contacttype.ContactType;
import com.softhub.chiremba.business.manufacturer.Manufacturer;
import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity(name = "manufacturer_contact")
public class ManufacturerContact extends BaseEntity {

    private String contactDetail;
    private boolean status = false;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Manufacturer manufacturer;

}
