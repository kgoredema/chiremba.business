package com.softhub.chiremba.business.manufacturercontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerContactRepository extends JpaRepository<ManufacturerContact, Long> {

}
