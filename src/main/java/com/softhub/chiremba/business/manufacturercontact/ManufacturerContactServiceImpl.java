package com.softhub.chiremba.business.manufacturercontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ManufacturerContactServiceImpl implements ManufacturerContactService {

    private final ManufacturerContactRepository manufacturerContactRepository;


    @Override
    public Optional<ManufacturerContact> get(Object id) {
        return manufacturerContactRepository.findById((Long) id);
    }

    @Override
    public List<ManufacturerContact> getAll() {
        return manufacturerContactRepository.findAll();
    }

    @Override
    public ManufacturerContact save(ManufacturerContact manufacturerContact) {
        return manufacturerContactRepository.save(manufacturerContact);
    }

    @Override
    public void delete(ManufacturerContact manufacturerContact) {
        manufacturerContactRepository.delete(manufacturerContact);

    }
}
