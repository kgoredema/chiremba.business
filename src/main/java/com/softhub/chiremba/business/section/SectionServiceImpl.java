package com.softhub.chiremba.business.section;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class SectionServiceImpl implements SectionService {

    private final SectionRepository sectionRepository;


    @Override
    public Optional<Section> get(Object id) {
        return sectionRepository.findById((Long) id);
    }

    @Override
    public List<Section> getAll() {
        return sectionRepository.findAll();
    }

    @Override
    public Section save(Section section) {
        return sectionRepository.save(section);
    }

    @Override
    public void delete(Section section) {
        sectionRepository.delete(section);

    }
}
