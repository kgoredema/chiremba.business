package com.softhub.chiremba.business.section;


import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "section")
public class Section extends BaseEntity {
    private String name;
    private String description;
    
}
