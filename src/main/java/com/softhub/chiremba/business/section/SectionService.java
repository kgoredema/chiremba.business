package com.softhub.chiremba.business.section;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface SectionService extends AppService<Section> {

}
