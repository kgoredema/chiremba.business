package com.softhub.chiremba.business.category;

import com.softhub.chiremba.business.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "category")
public class Category extends BaseEntity {

    private String name;
    private String description;

}
