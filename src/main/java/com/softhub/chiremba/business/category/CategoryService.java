package com.softhub.chiremba.business.category;

import com.softhub.chiremba.business.model.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CategoryService extends AppService<Category> {

}
